//import liraries
import React, { Component } from 'react';

// create a component
const Auth = () => {
 return (
  <div className="auth">
   <div className="container" id="container">
    <div className="form-container sign-up-container">
     Sign Up form code goes here
    </div>
    <div className="form-container sign-in-container">
     Sign In form code goes here
    </div>
    <div className="overlay-container">
     The overlay code goes here
    </div>
   </div>
  </div>
 );
};



//make this component available to the app
export default Auth;
