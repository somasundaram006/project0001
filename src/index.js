import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import Parse from 'parse';

//initialise the parse
Parse.initialize('myAppId');
// serverURL , if server hosted in cloud, then this will be changed
Parse.serverURL = "http://localhost:1337/parse"

ReactDOM.render(
  // <React.StrictMode>
    <App />
  // </React.StrictMode>
  ,document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
