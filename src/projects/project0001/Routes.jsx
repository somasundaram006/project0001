import React, { Suspense } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import ErrorBoundary from "../../utils/MyErrorBoundary";

import ScrollToTop from "../../utils/ScrollToTop";
import Loading from "../../utils/Loading";

const NotFound = React.lazy(() => { import('./NotFound'); });
const Home = React.lazy(() => { import('./Home'); });
const Auth = React.lazy(() => { import('../../components/Auth/Auth'); });
const LogOut = React.lazy(() => { import('../../components/Auth/LogOut'); });
const Routes = () => {
 return (
  <ErrorBoundary>
   <Suspense fallback={<Loading />} >
    <Router>
     <ScrollToTop>
      <Switch>
       <Route component={Home} path="/" exact
       />
       <Route component={Auth} path="/auth" exact
       />
       <Route component={LogOut} path="/logout" exact
       />
       <Route component={NotFound} path="/notfound" exact />
       <Redirect to="/notfound" />
      </Switch>
     </ScrollToTop>
    </Router>
   </Suspense>
  </ErrorBoundary>
 );
};
export default Routes;