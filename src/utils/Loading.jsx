//import liraries
import React, { Component } from 'react';
import { Spinner } from 'react-bootstrap';

// create a component
const Loading = () => {
 return (
  <Spinner animation="border" role="status">
   <span className="sr-only">Loading...</span>
  </Spinner>
 );
};
//make this component available to the app
export default Loading;
